app.controller('login', function ($rootScope, $scope, $http, $state, $httpParamSerializerJQLike) {
    $scope.login = {};
    $scope.submitLogin = function () {
        var data = $httpParamSerializerJQLike($scope.login);

        $http({
            method: 'POST',
            url: 'api/auth',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
            data: data
        })
            //Success
            .then(function (response) {
                $rootScope.token = localStorage.token = response.data;
                $state.go('app.home');
            },
            // Error
            function (error) {
                if (error.status === 400) {
                    swal('Oops', 'Esse email errado!', 'error');
                } else if(error.status === 401) {
                    swal('Oops', 'Seus dados estão incorretos!', 'error');
                }
            });
    };
});