app.controller('cadastrar', function ($scope, $http, $httpParamSerializerJQLike, $state) {
    $scope.submitRegister = function () {        
        $http({
            method: 'POST',
            url: 'api/auth/adicionar',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            },
            data : $httpParamSerializerJQLike($scope.cadastro)
        })
        //success
        .then(function(response){
            swal('Feito!', 'Seu cadastro foi efetuado com sucesso!', 'success');
            localStorage.token = response.data;
            $state.go('app.home');
        },
        //error
        function(error){
            if(error.status === 400){
                swal('Algo deu errado!', 'Parece que esse email já está cadastrado!','error');
                return;
            }
        });
    };

    $('#confirmPassword').on('keyup', function () {
        var senha = $scope.cadastro.password;
        var confirma = $scope.cadastro.rpassword;
        if(confirma != senha ) {
            console.log(confirma);
            console.log($scope.formCadastro);
            
        } else {
            console.log('senhas iguais');
        }
    });
});


