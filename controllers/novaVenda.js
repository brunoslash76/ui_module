app.controller('novaVenda', function ($scope, $rootScope, $http, $httpParamSerializerJQLike, $filter) {
    $scope.cliente = {};
    $scope.produto = {};
    $scope.venda = {};
    $scope.itensVenda = [];
    $scope.existe = false;
    $scope.cadastroClienteNV = {};

    $scope.pesquisarCliente = function () {
        if (!$scope.pesquisaCliente) {
            return;
        }
        var cpf  = $.param({
            "cpf" : $("#pesquisarCliente").cleanVal()
        });        
        $scope.cadastroClienteNV.cpf = $("#pesquisarCliente").val();

        $http({
            url: 'api/clientes/getByCPF',
            method: 'POST',
            headers: {
                'Content-Type' : 'application/x-www-form-urlencoded',
                'token' : localStorage.getItem('token')
            },
            data: cpf
        }).then(function (result) {
            $scope.cliente = result.data;
            $scope.existe = true;
            
        }, function (error) {
            if(error.status === 400) {
                swal({
                    title: 'Cliente não encontrado!',
                    text: 'Vamos cadastra-l!o',
                    type: 'warning',
                    allowEscapeKey: false
                })
                .then( (res) => {
                    $('#cadastrarnv').modal('toggle');
                });
                $scope.existe = false;
                $scope.pesquisaCliente = "";
            }
        });
        
    };//

    $scope.getProduto = function () {
        $http({
            url: 'api/produtos/get',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'token': localStorage.getItem('token')
            }
        }).then(function(result) {
            $scope.produtos = result.data;
            console.log(result.data);
        }, function(error){

        });
    };

    $scope.cadastrarCliente = function () {
        $scope.cadastrarCliente.cpf = $scope.nvcpf;
        var cliente = $httpParamSerializerJQLike($scope.cadastroClienteNV);
        $http({
            url: 'api/clientes/cadastrar',
            method: 'POST',
            headers: {
                'Content-Type':'application/x-www-form-urlencoded',
                'token': localStorage.getItem('token')
            },
            data: cliente
        }).then( function (res){

            $('#cadastrarnv').modal('toggle');
        }, function (error) {
            if(error.status === 400) {
                 swal({
                    title: 'Email já cadastrado',
                    text: 'Pesquise o email para corrigir o CPF!',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Verificar Email',
                    cancelButtonText: 'Cancelar'
                 }).then( () => {
                    $state.go('clientes');
                 });
            }
        });
    };

    $scope.adicionarProduto = function (produto) {
        produto.desliga = false;                
        if(produto.quantidade == 0) {
            produto.desliga = true;
            return;
        }
        --produto.quantidade;
        if(isNaN(produto.qtdOut)) {
            produto.qtdOut = 1;
        }
       

        //verifica se o produto já está na lista de compras
        let teste = $scope.itensVenda.some((element, index, array) => {
            return produto.DT_RowId == element.DT_RowId;
        });
        //adiciona +1 na quantidade caso o produto já esteja na lista e recalcula o preço total
        if(teste){
            let indiceEncontrado = $scope.itensVenda.findIndex((element) =>{
                return element.DT_RowId === produto.DT_RowId;
            });
            $scope.itensVenda[indiceEncontrado].qtdOut += 1;
            produto.precoTotal = parseFloat(produto.preco * produto.qtdOut);// recalcula o preço total           
        } else {
            produto.precoTotal = parseFloat(produto.preco * produto.qtdOut);// recalcula o preço total
            $scope.itensVenda.push(produto);//adiciona um produto que ainda não esteja na lista
        }
        // console.log($scope.itensVenda);
    };

    $scope.add = function(){
        console.log($scope.itensVenda);
        console.log($scope.cliente);

        // let cadastrar = $scope.itensVenda.filter((value)=>{

        // });

        // $http({
        //     url: 'api/vendas/adicionar',
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/x-www-form-urlencoded',
        //         'token' : localStorage.getItem('token')
        //     },
        //     data: $httpParamSerializerJQLike($scope.cliente)
        // }).then(function (result){
        //     console.log(result);
        // });
        
        // $http({
        //     url: 'api/itemVenda/adicionar',
        //     method: 'POST',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         'token': localStorage.getItem('token')
        //     },
        //     data: $scope.itensVenda
        // }).then(function(result) {
        //     console.log(result);
        // }, function(error){

        // });
    }

    $(".cpf-mask").mask("###.###.###-00");
});