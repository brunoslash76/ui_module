app.config(function ($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/cliente');
	$stateProvider
		// Dentro do app
		.state('app', {
			abstract: true,
			url: "",
			views: {
				'menu': {
					templateUrl: "views/menu.html?" + window.version,
					controller: 'menuLateral'
				},
				'header': {
					templateUrl: "views/header.html?" + window.version,
				},
				'root': {
					template: '<ng-view ui-view="view"></ng-view>',
					controller: "main"
				}
			}
		})
		.state('app.home', {
			url: '/home',
			views: {
				'view': {
					templateUrl: 'views/home.html',
					controller: 'home'
				}
			}
		})
		.state('app.perfil', {
			url: '/perfil',
			views: {
				'view': {
					templateUrl: 'views/perfil.html',
					controller: 'perfil'
				}
			}
		})
		.state('app.cliente', {
			url: '/cliente',
			views: {
				'view': {
					templateUrl: 'views/clientes.html',
					controller: 'cliente'
				}
			}
		})
		.state('app.produtos', {
			url: '/produtos',
			views: { 
				'view': {
					templateUrl: 'views/produtos.html',
					controller: 'produtos'
				}
			}
		})
		.state('app.vendas', {
			url: '/vendas',
			views: {
				'view': {
					templateUrl: 'views/vendas.html',
					controller: 'vendas'
				}
			}
		})
		.state('app.nova_venda', {
			url: '/nova_venda',
			views: {
				'view': {
					templateUrl: 'views/novaVenda.html',
					controller: 'novaVenda'
				}
			}
		})
		// FORA DO APP
		.state('acessibilidade', {
			abstract: true,
			url: '',
			views: {
				'root': {
					templateUrl: 'views/acessibilidade.html'
				}
			}
		})
		.state('acessibilidade.login', {
			url: '/login',
			views: {
				'access': {
					templateUrl: 'views/login.html',
					controller: 'login'
				}
			}
		})
		.state('acessibilidade.cadastrar', {
			url: '/cadastrar',
			views: {
				'access': {
					templateUrl: 'views/cadastrar.html',
					controller: 'cadastrar'
				}
			}
		})
		.state('acessibilidade.recuperar', {
			url: '/recuperar',
			views: {
				'access': {
					templateUrl: 'views/recuperar.html',
					controller: 'recuperar'
				}
			}
		});
});