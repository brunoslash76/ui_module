<?php use Mailgun\Mailgun;
 class Clientes extends CI_Controller{
	
	public function verificarToken(){
		$this->AuthUser->autenticarUsuario($this->input->get_request_header('token'));
	}

	//Autorizar o login do usuário
	public function autorizar(){
		$this->verificarToken();
		$res = $this->db->set('ativo', 1)->where('token', $this->input->post('token'))->update('clientes');
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}		
		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function enviarEmail(){
		$this->verificarToken();
		$usuario = $this->db->select('name, email, ativo')->where('token', $this->input->post('token'))->get('clientes')->row();
		# First, instantiate the SDK with your API credentials
		$mg = Mailgun::create('key-4f5c5c70ab45c128df806e89628be784');
		$domain = "bitmi.com.br";
		if($usuario->ativo == 0){
			$mg->messages()->send($domain, [
			  'from'    => 'postmaster@bitmi.com.br',
			  'to'      => $usuario->email,
			  'subject' => 'MisterLeilão Status',
			  'text'    => $usuario->name . ', sua conta foi bloqueada pelo administrador'
			]);
		}else{
			$mg->messages()->send($domain, [
			  'from'    => 'postmaster@bitmi.com.br',
			  'to'      => $usuario->email,
			  'subject' => 'MisterLeilão Status',
			  'text'    => $usuario->name . ', sua conta foi ativada pelo administrador, você já pode utilizar seu email(' . $usuario->email . ') para utilizar o sistema'
			]);
		}		
	}

	//Desautorizar o login do usuário
	public function desautorizar(){
		$this->verificarToken();
		$res = $this->db->set('ativo', 0)->where('token', $this->input->post('token'))->update('clientes');
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}		
		return $this->output->set_content_type('application/json')->set_output(json_encode($res));

	}

	public function atualizar(){
		$this->verificarToken();
		$update = [
			'name' => $this->input->post('name'),
			'cpf' => $this->input->post('cpf'),
			'email' => $this->input->post('email'),
			'phoneNumber' => $this->input->post('phoneNumber'),
			'token' => password_hash($this->input->post('email'), PASSWORD_DEFAULT)];

			
			$res = $this->db->where('token', $this->input->post('token'))
				->update('clientes', $update);
			if(!$res){
				return $this->output->set_content_type('application/json')->set_status_header(400);	
				exit();
			}		
			return $this->output->set_content_type('application/json')->set_output(json_encode($update['token']));	
	}

	//Para para popular o Datatables
	public function get(){
		$this->verificarToken();
		$res = $this->db->select('id as DT_RowId, name, email, cpf, phoneNumber, token, ativo')->where('token !=', $this->input->get_request_header('token'))->get('clientes')->result();
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function getByCPF(){		
		$this->verificarToken();			
		$cpf = $this->input->post('cpf');	
		$res = $this->db->get_where('clientes', array('cpf' => $cpf))->row(); 	
		if(!$res){			
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();			
		}
		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
		
	}

	public function deletar(){
		$this->verificarToken();
		$res = $this->db->where('token', $this->input->post('token'))->delete('clientes');
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		return $this->output->set_content_type('application/json')->set_output($res);
	}	

	public function adicionar(){
		//dados para o insert		
		$this->verificarToken();
		$user = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'token' => password_hash($this->input->post('email'), PASSWORD_DEFAULT),
			'cpf' => $this->input->post('cpf'),
			'phoneNumber' => $this->input->post('phoneNumber'),
			'ativo' => $this->input->post('ativo')
		);
		if($this->db->where('email', $user['email'])->get('clientes')->row() || $this->db->where('cpf', $user['cpf'])->get('clientes')->row()) {
			return $this->output->set_content_type('application/json')->set_status_header(400);	     
			exit();
		}	
		$this->db->insert('clientes', $user);
		return $this->output->set_content_type('application/json')->set_output(json_encode($user['token']));	
	}
}?>