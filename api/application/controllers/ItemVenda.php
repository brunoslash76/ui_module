<?php class ItemVenda extends CI_Controller{
	public function verificarToken() {
        $this->AuthUser->autenticarUsuario($this->input->get_request_header('token'));
    }

    public function adicionar(){
    	$this->verificarToken();
    	$in = json_decode(trim(file_get_contents('php://input')), true);// in é(devia ser) um array de item_vendas com id_produto e quantidade       

    	foreach($in as $key => $dados){
    		$in[$key]['id_venda'] = $this->db->insert_id();
    		$produto = $this->db->get_where('produtos', array('id' => $in[$key]['id_produto']))->row();  //seleciona o produto para atualizar a quantidade
    		$qtdAtualizada = $produto->quantidade - $in[$key]['qtd']; //quantidade atual - quantiadade vendida  	
    		
    		if($qtdAtualizada < 1){// retorna 403 caso não haja produtos em estoque
	    		return $this->output->set_content_type('application/json')->set_status_header(403);
	    		exit();
	    	}
	    	// $in[$key]['sub_total'] = $produto->preco * $in[$key]['qtd'];
	    	$this->db->set('quantidade', $qtdAtualizada)->where('id', $in[$key]['id_produto'])->update('produtos');//atualiza a quantidade de produtos na tabela produtos
    	}  	    
    	
    	$res = $this->db->insert_batch('item_venda', $in); //insere tudo de uma vez
    	if($res){
    		$venda = $this->db->select('*')->where('id', $in[0]['id_venda'])->get('vendas')->row();//seleciona a venda que foi feita
    		$sub_total = $this->db->select('sum(sub_total) as sub_total')->where('id_venda', $venda->id)->get('item_venda')->row();//seleciona o total da soma do sub_total de todos os itens vendidos da venda que foi feita
    		$total = $venda->total + $sub_total->sub_total;
    		if(!$this->db->set('total', $total)->where('id', $venda->id)->update('vendas')) {//atualiza o valor total da venda e a hora com current_timestamp do mysql
    			return $this->output->set_content_type('application/json')->set_status_header(400);
    			exit();
    		}
    	}
    	return $this->output->set_content_type('application/json')->set_output(json_encode($res));   	
    }

    public function get(){//seleciona todos os itens vendidos de uma venda
    	$this->verificarToken();
    	$id = $this->input->post('id_venda');
    	$res = $this->db->select('iv.id_venda, iv.id_produto, p.nome, p.fabricante, iv.qtd, iv.sub_total')->from('item_venda iv')
    		->join('produtos p', 'iv.id_produto = p.id')->where('iv.id_venda', $id)->get()->result();
    	if(!$res){
    		return $this->output->set_content_type('application/json')->set_status_header(400);
    		exit();
    	}
    	return  $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}?>