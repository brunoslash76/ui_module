<?php class Vendas extends CI_Controller{
	

	public function verificarToken() {
        return $this->AuthUser->autenticarUsuario($this->input->get_request_header('token'));
    }

	public function get(){
		$this->verificarToken();
		$res = $this->db->select('v.id, c.id as id_cliente, c.name as cliente, a.id as id_vendedor, a.name as vendedor, v.dt_venda, v.total')->from('vendas v')->join('clientes c', 'c.id = v.id_cliente')
			->join('admin a', 'a.id = v.id_vendedor')->get()->result();

		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function adicionar(){
		$user = $this->verificarToken();	//retorna o vendedor como um stdClass
		$venda = $this->input->post();
		$venda['dt_venda'] = date("Y-m-d H:i:s");
		$venda['id_vendedor'] = $user->id;
		$venda['id_cliente'] = $venda['id'];
		$res = $this->db->insert('vendas', $venda);
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		return  $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function atualizarStatus(){
		$user = $this->verificarToken();
		$att = $this->input->post(); // recebe o id e o novo status
		$res = $this->db->set('status', $att['status'])->where('id', $att['id'])->update('vendas');
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		return  $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function cancelarVenda(){
		$user = $this->verificarToken();
		$id_venda = $this->input->post('id_venda');

		//seleciona os itens vendidos da venda a ser cancelada
		$itens = $this->db->select('*')->where('id_venda', $id_venda)->get('item_venda')->result();
		foreach($itens as $item){
			$id_produto = $item->id_produto;
			$produtoEstoque = $this->db->select('id, quantidade')->where('id', $id_produto)->get('produtos')->row();//seleciona a quantidade em estoque dos produtos vendidos

			$qtd = $item->qtd; // quantidade vendida para fazer o "estorno" no banco de dados

			$qtdSoma = $qtd + $produtoEstoque->quantidade; //soma a quantidade vendida com a quantidade atual em estoque

			//Atualiza a quantidade de produtos em estoque
			if(!$this->db->set('quantidade', $qtdSoma, false)->where('id',  $produtoEstoque->id)->update('produtos')){
				return $this->output->set_content_type('application/json')->set_status_header(400);
				exit();
			}					
		}

		$vendaStatus = $this->db->select('status')->where('id', $id_venda)->get('vendas')->row();
		if($vendaStatus->status == 'Cancelada'){
			return $this->output->set_content_type('application/json')->set_status_header(412);//precondição falhou
			exit();
		}

		//atualiza o status da venda para Cancelada
		$res = $this->db->set('status', 'Cancelada')->where('id', $id_venda)->update('vendas');
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		return  $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}
}?>