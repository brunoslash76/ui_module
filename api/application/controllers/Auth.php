<?php 
class Auth extends CI_Controller{
	//INDEX = LOGIN
	public function index(){
		$res = $this->db->select('token, password')->where('email', $this->input->post('email'))->get('admin')->row();
		if(!$res){
			return $this->output->set_content_type('application/json')->set_status_header(400);     
			exit();
		}
		if(!password_verify($this->input->post('password'), $res->password)){
			return $this->output->set_content_type('application/json')->set_status_header(401);     
			exit();
		}		
		return $this->output->set_content_type('application/json')->set_output(json_encode($res->token));
	}

	public function adicionar(){
		$add = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'token' => password_hash($this->input->post('email'), PASSWORD_DEFAULT)
		);
		if($this->db->where('email', $add['email'])->get('admin')->row()) {
			return $this->output->set_content_type('application/json')->set_status_header(400);	     
			exit();
		}	
		$this->db->insert('admin', $add);
		return $this->output->set_content_type('application/json')->set_output(json_encode($add['token']));	
	}

	public function verificarEmail(){
		//Verifica se o email está cadastrado
		if(!$this->db->where('email', $this->input->post('email'))->get('admin')->row()){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}
		//gera um token de verificação baseado no tempo atual em milliseconds
		$token = password_hash(date("U"), PASSWORD_DEFAULT);
		$this->session->set_userdata('tokenRecovery', $token);
		return $this->output->set_content_type('application/json')->set_output(json_encode($token));
	}

	//valida se o token recebido é o mesmo que foi gerado
	public function verificarToken(){
		if(!$this->input->post('token') === $this->session->userdata('tokenRecovery')){
			return $this->output->set_content_type('application/json')->set_status_header(400);	
			exit();
		}
		return $this->output->set_content_type('application/json');
	}	

	public function alterarSenha(){		
		if(!$this->db->where('email', $this->input->post('email'))->get('admin')->row()){
			return $this->output->set_content_type('application/json')->set_status_header(400);		
			exit();
		}

		if(!$this->input->post('token') === $this->session->userdata('tokenVerificacao')){
			return $this->output->set_content_type('application/json')->set_status_header(401);		
			exit();
		}
		//dados da nova senha e do novo token
		$admin = ['password' => password_hash($this->input->post('npassword'), PASSWORD_DEFAULT),'token' => password_hash($this->input->post('email'), PASSWORD_DEFAULT)];
		$this->db->where('email',  $this->input->post('email'))->update('admin', $admin);
		return $this->output->set_content_type('application/json')
			->set_output(json_encode($user['token']));
	}
}?>