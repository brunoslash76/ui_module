<?php class Profile extends CI_Controller{
	//Essa função retorna os dados do usuário logado
	public function index(){
		$this->verificarToken();
		$res = $this->db->select('name, email')->where('token', $this->input->get_request_header('token'))->get('admin')->row();		
		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}

	public function verificarToken(){
		$this->AuthUser->autenticarUsuario($this->input->get_request_header('token'));
	}

	public function atualizar(){
		$this->verificarToken();
		$update = [
			'name' => $this->input->post('fullname'),
			'cpf' => $this->input->post('cpf'),
			'phoneNumber' => $this->input->post('phoneNumber')];

			$res = $this->db->where('token', $this->input->get_request_header('token'))
				->update('clientes', $update);
			if(!$res){
				return $this->output->set_content_type('application/json')->set_status_header(400);	
				exit();
			}
		
			return $this->output->set_content_type('application/json')->set_output(json_encode($this->input->get_request_header('token')));	
	}

	public function atualizarEmail(){
		$this->verificarToken();
		if($this->db->where('email', $this->input->post('email'))->get('clientes')->row()){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}		

		//Recupera os dados a serem atualizados (email) e atualiza o token a partir do novo email
		$att = [
			'email' => $this->input->post('email'),
			'token' => password_hash($this->input->post('email'), PASSWORD_DEFAULT)];

		if(!$this->db->where('token', $this->input->get_request_header('token'))->update('clientes', $att)){
			return $this->output->set_content_type('application/json')->set_status_header(400);
			exit();
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($att->token));
	}
}?>