# Customer Module

This module provides system admin to control users access to the system. This is part of a bigger software.

## How to install

1. Download AMP, MAMP or XAMP to be the container for this application.
2. Inside MAMP folders find `htdocs` and clone the project there.
3. Install MySqlWorkbench on your machine to have an User Interface for you database.
4. Open the project and search for `misterleilaoadmin.sql`, copy and past to your workbench project. Remember to start MAMP and set the connections to MySql from MAMP.
5. Run the script to create all databases, tables and populate database with some example data.
6. On your project search for the folder `api` then go to `config` and inside this folder open `database.php` set the the name of database connection name, password for database.
7. Restart MAMP and access [localhost:8888/management_module](http://localhost:8888/management_module)




### What is this repository for? ###

This is a portif�lio repository.
I am using AngularJS, CodeIgniter and MySQL, Bootstrap and Porto template.

### Contribution guidelines ###

Front End Dev: Bruno de Moraes
Email: btmrusso@gmail.com

Backend programer Elton Bezerra 
Email: elton.cin@hotmail.com

